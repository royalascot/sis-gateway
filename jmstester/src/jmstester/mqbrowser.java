package jmstester;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnection;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.mq.jms.MQQueueReceiver;
import com.ibm.mq.jms.MQQueueSender;
import com.ibm.mq.jms.MQQueueSession;

/**
 * A minimal and simple application for Point-to-point messaging.
 * 
 * Application makes use of fixed literals, any customisations will require
 * re-compilation of this source file. Application assumes that the named queue
 * is empty prior to a run.
 * 
 * Notes:
 * 
 * API type: IBM WebSphere MQ JMS API (v1.02, domain specific)
 * 
 * Messaging domain: Point-to-point
 * 
 * Provider type: WebSphere MQ
 * 
 * Connection mode: Client connection
 * 
 * JNDI in use: No
 * 
 */
@SuppressWarnings("deprecation")
public class mqbrowser {

	// System exit status value (assume unset value to be 1)
	private static int status = 1;

	private static String host = "SISData365-acpt.sis.tv";
	private static int port = 1438;
	private static String channel = "S_BETFRDTST1";
	private static String queueManagerName = "ASEGBFRD5";
	private static String queueName = "SPORTSDATA_11";
	private static boolean clientTransport = true;

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Variables
		MQQueueConnection connection = null;
		MQQueueSession session = null;
		MQQueue queue = null;
		MQQueueSender sender = null;
		MQQueueReceiver receiver = null;

		try {
			// Create a connection factory
			MQQueueConnectionFactory cf = new MQQueueConnectionFactory();

			// Set the properties

			System.setProperty("javax.net.ssl.keyStore","E:\\ssl\\betfrdtst1.key.jks");
		    System.setProperty("javax.net.ssl.trustStore","E:\\ssl\\betfrdtst1.key.jks");
		    System.setProperty("javax.net.ssl.trustStorePassword", "betfrdtst1");
		    System.setProperty("javax.net.ssl.keyStorePassword", "betfrdtst1");

			cf.setHostName(host);
			cf.setPort(port);
			cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
			cf.setQueueManager(queueManagerName);
			cf.setChannel(channel);
			cf.setSSLCipherSuite("SSL_RSA_WITH_DES_CBC_SHA");
			cf.setSSLFipsRequired(false);

			// Create JMS objects
			connection = (MQQueueConnection) cf.createQueueConnection();
			session = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = (MQQueue) session.createQueue("queue:///Q1");
			sender = (MQQueueSender) session.createSender(queue);
			receiver = (MQQueueReceiver) session.createReceiver(queue);

			long uniqueNumber = System.currentTimeMillis() % 1000;
			TextMessage message = session.createTextMessage("SimpleWMQJMSPTP: Your lucky number today is " + uniqueNumber);

			// Start the connection
			connection.start();

			// And, send the message
			sender.send(message);
			System.out.println("Sent message:\n" + message);

			// Now, receive the message
			Message receivedMessage = receiver.receive(15000); // in ms or 15
																// seconds
			System.out.println("\nReceived message:\n" + receivedMessage);

			recordSuccess();
		} catch (JMSException jmsex) {
			recordFailure(jmsex);
		} finally {
			if (sender != null) {
				try {
					sender.close();
				} catch (JMSException jmsex) {
					System.out.println("Sender could not be closed.");
					recordFailure(jmsex);
				}
			}
			if (receiver != null) {
				try {
					receiver.close();
				} catch (JMSException jmsex) {
					System.out.println("Receiver could not be closed.");
					recordFailure(jmsex);
				}
			}

			if (session != null) {
				try {
					session.close();
				} catch (JMSException jmsex) {
					System.out.println("Session could not be closed.");
					recordFailure(jmsex);
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException jmsex) {
					System.out.println("Connection could not be closed.");
					recordFailure(jmsex);
				}
			}
		}
		System.exit(status);
		return;
	} // end main()

	/**
	 * Process a JMSException and any associated inner exceptions.
	 * 
	 * @param jmsex
	 */
	private static void processJMSException(JMSException jmsex) {
		System.out.println(jmsex);
		Throwable innerException = jmsex.getLinkedException();
		if (innerException != null) {
			System.out.println("Inner exception(s):");
		}
		while (innerException != null) {
			System.out.println(innerException);
			innerException = innerException.getCause();
		}
		return;
	}

	/**
	 * Record this run as successful.
	 */
	private static void recordSuccess() {
		System.out.println("SUCCESS");
		status = 0;
		return;
	}

	/**
	 * Record this run as failure.
	 * 
	 * @param ex
	 */
	private static void recordFailure(Exception ex) {
		if (ex != null) {
			if (ex instanceof JMSException) {
				processJMSException((JMSException) ex);
			} else {
				System.out.println(ex);
			}
		}
		System.out.println("FAILURE");
		status = -1;
		return;
	}

}
