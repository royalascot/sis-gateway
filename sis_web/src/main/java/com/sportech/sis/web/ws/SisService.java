package com.sportech.sis.web.ws;

import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

@Path("/")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@Named("SisService")
public class SisService {

	static private final Logger log = Logger.getLogger(SisService.class);

	@GET
	@Path("/version")
	public String getVersion(@Context ServletContext context) {
		try {
			InputStream inputStream = context.getResourceAsStream("/META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(inputStream);
			Attributes attrs = manifest.getMainAttributes();
			if (attrs != null) {
				String version = attrs.getValue("Application-Version");
				String buildNumber = attrs.getValue("Build-Number");
				if (!StringUtils.isEmpty(version)) {
					if (!StringUtils.isEmpty(buildNumber)) {
						version += " build " + buildNumber;
					}
					return version;
				}
			}
		} catch (Exception ex) {
			log.error("Error getting version number.", ex);
		}

		return "0.1";
	}

}