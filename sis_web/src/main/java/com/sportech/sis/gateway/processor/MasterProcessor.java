package com.sportech.sis.gateway.processor;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import org.apache.log4j.Logger;

import com.sportech.common.actionrule.RuleExecutor;
import com.sportech.sis.entity.MasterSnapshot;
import com.sportech.sis.entity.RawMessage;

public class MasterProcessor {

	static private final Logger log = Logger.getLogger(MasterProcessor.class);

	private RuleExecutor<ProcessorBase> executor = null;

	public MasterProcessor() {
		executor = new RuleExecutor<ProcessorBase>("com.sportech.sis.gateway.processor.master", ProcessorBase.class);
	}

	public void process(EntityManager em, RawMessage message) {
		ProcessingContext context = new ProcessingContext();
		context.setMessage(message);
		context.setEntityManager(em);

		MasterSnapshot master = new MasterSnapshot();
		master.setMessageDate(message.getMessageDate());
		master.setMessageType(message.getMessageType());
		master.setRaceType(message.getRaceType());
		master.setLastUpdate(message.getMessageTimestamp());
		master.setText(message.getText());
		master.setSisId(message.getSisId());
		em.persist(master);
		em.lock(master, LockModeType.PESSIMISTIC_WRITE);
		context.setMaster(master);
		int ruleCount = executor.execute(context);
		if (ruleCount < 1) {
			log.warn("No master processor found for type:" + message.getMessageType());
		} 
	}

}
