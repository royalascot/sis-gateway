package com.sportech.sis.gateway.util;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.sportech.sis.entity.MasterSnapshot;
import com.sportech.sis.entity.RawMessage;
import com.sportech.sis.gateway.dao.MessageDao;
import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.xmlupdater.XmlUpdater;

public class UpdateHelper {

	static private final Logger log = Logger.getLogger(UpdateHelper.class);

	private static long UPDATE_TIME_LIMIT = 60000L;

	public static boolean applyUpdate(ProcessingContext context) {

		RawMessage raw = context.getMessage();
		String messageDate = raw.getMessageDate();
		MessageDao dao = new MessageDao(context.getEntityManager());
		String[] messageIds = StringUtils.split(raw.getSisId(), ",");
		if (messageIds != null) {
			for (String sisId : messageIds) {
				MasterSnapshot master = dao.findMaster(messageDate, sisId);
				if (master == null) {
					log.error("Can not find master entry for update message:" + raw.getSisId());
					return false;
				}
				if (master.getLastUpdate() - raw.getMessageTimestamp() > UPDATE_TIME_LIMIT) {
					log.warn("Ignore update for:" + sisId + "-" + master.getLastUpdate() + ">" + raw.getMessageTimestamp());
					return false;
				}
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document document = builder.parse(IOUtils.toInputStream(master.getText(), "UTF-8"));
					XmlUpdater updater = new XmlUpdater();
					updater.updateMaster(document, raw.getText());

					TransformerFactory tFactory = TransformerFactory.newInstance();
					Transformer transformer = tFactory.newTransformer();
					DOMSource source = new DOMSource(document);
					StringWriter writer = new StringWriter();
					StreamResult result = new StreamResult(writer);
					transformer.transform(source, result);
					master.setText(writer.getBuffer().toString());
					if (raw.getMessageTimestamp() > master.getLastUpdate()) {
						master.setLastUpdate(raw.getMessageTimestamp());
					}
					context.getEntityManager().persist(master);
					context.setMaster(master);
				} catch (Exception e) {
					log.error("Error applying update", e);
					return false;
				}
			}
		}
		return true;
	}

}
