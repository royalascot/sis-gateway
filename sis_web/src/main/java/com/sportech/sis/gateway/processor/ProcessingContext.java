package com.sportech.sis.gateway.processor;

import javax.persistence.EntityManager;

import com.sportech.common.actionrule.RuleContext;
import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.entity.MasterSnapshot;
import com.sportech.sis.entity.RawMessage;

public class ProcessingContext implements RuleContext {

	private RawMessage message;

	private EntityManager entityManager;

	private EventCard card;

	private MasterSnapshot master;

	public RawMessage getMessage() {
		return message;
	}

	public void setMessage(RawMessage message) {
		this.message = message;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EventCard getCard() {
		return card;
	}

	public void setCard(EventCard card) {
		this.card = card;
	}

	public MasterSnapshot getMaster() {
		return master;
	}

	public void setMaster(MasterSnapshot master) {
		this.master = master;
	}

}
