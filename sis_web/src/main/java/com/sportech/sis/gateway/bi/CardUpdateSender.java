package com.sportech.sis.gateway.bi;

import com.sportech.common.model.entity.EventCard;

public interface CardUpdateSender {

	public void sendObject(EventCard object);

}
