package com.sportech.sis.gateway.processor.update;

import java.io.StringReader;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.gateway.processor.ProcessorBase;
import com.sportech.sis.gateway.util.EntityHelper;
import com.sportech.sis.gateway.util.NonRunnerImportHelper;
import com.sportech.sis.gateway.util.UpdateHelper;

public class NonRunnerUpdate extends ProcessorBase {

	static private final Logger log = Logger.getLogger(NonRunnerUpdate.class);

	@Override
	public String getMessageType() {
		return "NR";
	}

	@Override
	public void execute(ProcessingContext context) {
		try {
			if (UpdateHelper.applyUpdate(context)) {
				NonRunnerImportHelper helper = new NonRunnerImportHelper();
				EventCard newCard = helper.parse(context.getEntityManager(), new StringReader(context.getMaster().getText()));
				if (newCard != null) {
					context.setCard(newCard);
					EntityHelper.updateTimestamp(newCard);
					context.getEntityManager().persist(newCard);
				}
			}
		} catch (Exception e) {
			log.error("Error applying update message", e);
		}
	}

}
