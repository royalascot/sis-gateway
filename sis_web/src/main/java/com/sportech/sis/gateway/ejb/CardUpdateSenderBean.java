package com.sportech.sis.gateway.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.gateway.bi.CardUpdateSender;

@Stateless
@Local
@LocalBean
public class CardUpdateSenderBean implements CardUpdateSender {

    static private final Logger log = Logger.getLogger(CardUpdateSenderBean.class);

    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/jms/queue/sisUpdate")
    private Destination queue;

    private Connection connection;
    private Session session = null;
    private MessageProducer sender = null;

    @PostConstruct
    public void init() {
        connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            sender = session.createProducer(queue);
            sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        } catch (Exception e) {
            log.error("Error connecting to queue", e);
        }       
    }

    @PreDestroy
    public void destroy() {
        try {
            if (sender != null) {
                sender.close();
            }
            if (session != null) {
                session.close();
            }
        } catch (Exception e) {
            log.error("error close", e);
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                log.error("Error closing connetion", e);
            }
        }
    }

    public void send(String text) {
        try {
            TextMessage response = session.createTextMessage(text);
            sender.send(response);
        } catch (Exception e) {
            log.error("Error sending message", e);
        }
    }

    public void sendObject(EventCard c) {
        try {
            ObjectMessage response = session.createObjectMessage(c);
            sender.send(response);
        } catch (Exception e) {
            log.error("Error sending message", e);
        }
    }

}