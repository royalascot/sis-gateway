package com.sportech.sis.gateway.processor.update;

import java.io.StringReader;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.gateway.processor.ProcessorBase;
import com.sportech.sis.gateway.util.EntityHelper;
import com.sportech.sis.gateway.util.EventImportHelper;
import com.sportech.sis.gateway.util.UpdateHelper;

public class EventUpdate extends ProcessorBase {

	static private final Logger log = Logger.getLogger(EventUpdate.class);

	@Override
	public String getMessageType() {
		return "ES";
	}

	private EventImportHelper helper = new EventImportHelper();

	@Override
	public void execute(ProcessingContext context) {
		try {
			if (UpdateHelper.applyUpdate(context)) {
				EventCard newCard = helper.parse(context.getEntityManager(), new StringReader(context.getMaster().getText()));
				if (newCard != null) {
					context.setCard(newCard);
					EntityHelper.updateTimestamp(newCard);
					context.getEntityManager().persist(newCard);
				}
				else {
					log.warn("Empty update action.");
				}
			}
		} catch (Exception e) {
			log.error("Error applying update message", e);
		}

	}

}
