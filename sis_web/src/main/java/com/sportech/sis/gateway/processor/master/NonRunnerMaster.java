package com.sportech.sis.gateway.processor.master;

import org.apache.log4j.Logger;

import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.gateway.processor.ProcessorBase;

public class NonRunnerMaster extends ProcessorBase {
	
	static private final Logger log = Logger.getLogger(NonRunnerMaster.class);

	@Override
	public String getMessageType() {
		return "NR";
	}

	@Override
	public void execute(ProcessingContext context) {
		log.info("Got master non-runner message");
	}
	
}
