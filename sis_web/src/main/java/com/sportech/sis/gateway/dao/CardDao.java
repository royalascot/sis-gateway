package com.sportech.sis.gateway.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;

public class CardDao {
	static private final Logger log = Logger.getLogger(CardDao.class);

	private EntityManager em;

	public CardDao(EntityManager em) {
		this.em = em;
	}

	public EventCard findCardByCode(String date, String code) {
		String jql = "FROM EventCard WHERE cardDate = :date AND sourceId = :code";
		TypedQuery<EventCard> query = em.createQuery(jql, EventCard.class);
		query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
		query.setParameter("date", date);
		query.setParameter("code", code);
		List<EventCard> results = query.getResultList();
		if (results != null && results.size() > 0) {
			if (results.size() > 1) {
				log.warn("non-unique card entries found");
			}
			return results.get(0);
		}
		return null;
	}

    public List<EventCard> getCardsByTimestamp(long lastUpdate) {
        TypedQuery<EventCard> query = em.createQuery("FROM EventCard c WHERE c.lastUpdate > :lastUpdate", EventCard.class);
        query.setParameter("lastUpdate", lastUpdate);
        List<EventCard> results = query.getResultList();
        return results;
    }

    public EventCard getFullCard(Long id) {
        TypedQuery<EventCard> query = em.createQuery("FROM EventCard c LEFT OUTER JOIN FETCH c.races r LEFT OUTER JOIN FETCH r.runners b LEFT OUTER JOIN FETCH c.pools WHERE c.id = :id", EventCard.class);
        query.setParameter("id", id);
        List<EventCard> results = query.getResultList();
        if (results != null && results.size() > 0) {
            return results.get(0);
        }
        return null;
    }

}
