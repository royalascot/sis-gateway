package com.sportech.sis.gateway.util;

import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.entity.BetInterest;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventRace;
import com.sportech.common.util.DateHelper;
import com.sportech.sis.common.ImportException;
import com.sportech.sis.common.SisParserHelper;
import com.sportech.sis.gateway.dao.CardDao;
import com.sportech.sis.schema.es.BoolEnumeration;
import com.sportech.sis.schema.es.CategoryEnum;
import com.sportech.sis.schema.es.DataContent;
import com.sportech.sis.schema.es.EventContent;
import com.sportech.sis.schema.es.MeetingContent;
import com.sportech.sis.schema.es.Price;
import com.sportech.sis.schema.es.SelectionContent;

public class EventImportHelper {

	static private final Logger log = Logger.getLogger(EventImportHelper.class);

	private static final String WEIGHT_UNIT = "lb";

	public EventCard parse(EntityManager em, Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(reader), DataContent.class);
			EventCard card = importCard(em, o.getValue());
			return card;
		} catch (Exception e) {
			log.error("Error importing event message", e);
		}
		return null;
	}

	private BetInterest importRunner(EventRace race, SelectionContent starter) throws ImportException {
		BetInterest runner = EntityHelper.findRunner(race, starter.getNum().intValue());
		if (runner == null) {
			runner = new BetInterest();
			runner.setPosition(starter.getNum().longValue());
			runner.setRace(race);
			race.getRunners().add(runner);
		}
		
		try {

			runner.setSourceId("" + starter.getId());
			runner.setJockeyName(starter.getJockey());
			runner.setTrainer(starter.getTrainer());
			runner.setStatus(starter.getStatus());
			runner.setName(starter.getName());
			runner.setShortName(SisParserHelper.getRunnerShortName(runner.getName()));
			int weight = 0;
			if (starter.getWeightStones() != null) {
				weight = starter.getWeightStones().intValue() * 14;
				runner.setWeightStones(starter.getWeightStones().intValue());
			}
			if (starter.getWeightPounds() != null) {
				weight += starter.getWeightPounds().intValue();
				runner.setWeightPounds(starter.getWeightPounds().intValue());
			}
			if (weight > 0) {
				runner.setWeightCarried("" + weight);
				runner.setWeightUnits(WEIGHT_UNIT);
			}
			runner.setScratched(false);
			runner.setProgramNumber("" + starter.getNum());
			
			runner.setCoupleIndicator(starter.getCoupled());

			for (Price p : starter.getPrice()) {
				runner.setDec(p.getDec());
				runner.setFract(p.getFract());
				runner.setMornlineOdds(p.getFract());
				if (p.getTimestamp() != null) {
					runner.setPriceTimestamp(p.getTimestamp().longValue());
				}
				if (p.getMktnum() != null) {
					runner.setMarketNum(p.getMktnum().longValue());
				}
				runner.setMarketType(p.getMkttype());
			}

			return runner;

		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private EventRace importRace(EventCard card, MeetingContent meeting, EventContent race) throws ImportException {
		EventRace hr = EntityHelper.findRace(card, race.getNum().intValue());
		if (hr == null) {
			hr = new EventRace();
			hr.setNumber(race.getNum().longValue());
			hr.setCard(card);
			card.getRaces().add(hr);
			hr.setRaceCode(SisParserHelper.getRaceCode(meeting.getSubcode()));
		}

		try {
			hr.setCoverageCode(race.getCoverageCode());
			hr.setGrade(race.getGrade());
			hr.setGoing(race.getGoing());
			if (race.getEachWayPlaces() != null) {
				hr.setEachWayPlaces(race.getEachWayPlaces().intValue());
			}
			if (race.getPlacesExpected() != null) {
				hr.setPlacesExpected(race.getPlacesExpected().intValue());
			}
			hr.setSurface("" + race.getSurface());
			hr.setProgressCode(race.getProgressCode());
			hr.setNumber(race.getNum().longValue());
			hr.setDistance(race.getDistance());
			hr.setRaceType(race.getGrade());
			hr.setDistanceText(race.getDistance());
			if (race.getHandicap() != null) {
				hr.setHandicap(BoolEnumeration.Y.equals(race.getHandicap()));
			}

			hr.setSourceId("" + race.getId());
			hr.setSourceType("Race");
			if (race.getCourseType() != null) {
				hr.setCourse(race.getCourseType().toString());
			}
			if (race.getSurface() != null) {
				hr.setSurface(race.getSurface().toString());
			}
			if (race.getTime() != null) {
				hr.setPostTime(DateHelper.getPostTime(race.getTime()));
			}
			hr.setRaceNameLong(race.getName());
			hr.setRaceNameShort(StringUtils.abbreviate(race.getName(), 20));

			for (SelectionContent s : race.getSelection()) {
				importRunner(hr, s);
			}

			SisParserHelper.parsePool(race, hr);

		} catch (Exception e) {
			log.error("Error importing race", e);
			throw new ImportException("Error importing race.");
		}
		return hr;
	}

	private void importMeeting(MeetingContent meeting, EventCard card) throws ImportException {
		try {
			card.setCoverageCode(meeting.getCoverageCode());
			card.setGoing(meeting.getGoing());
			card.setName(meeting.getName());
			card.setStatus(meeting.getStatus());
			card.setSubCode(meeting.getSubcode());
			card.setRacingStatus(meeting.getGoing());
			card.setCardDate(DateHelper.dateToStr(DateHelper.getCardDate(meeting.getDate())));
			card.setDescription(meeting.getName());
			card.setSourceId(meeting.getCode());
			card.setSourceType("Meeting");

			if (meeting.getEvent() != null) {
				if (card.getRaces() == null) {
					Set<EventRace> races = new HashSet<EventRace>();
					card.setRaces(races);
				}
				importRace(card, meeting, meeting.getEvent());
			}
		} catch (Exception e) {
			log.error("Error importing meeting", e);
			throw new ImportException("Error importing races.");
		}
	}

	private EventCard importCard(EntityManager em, DataContent raceCard) throws ImportException {
		if (raceCard.getMeeting() == null) {
			return null;
		}
		String cardDate = DateHelper.dateToStr(DateHelper.getCardDate(raceCard.getDate()));
		String code = raceCard.getMeeting().getCode();
		EventCard card = new CardDao(em).findCardByCode(cardDate, code);
		try {

			if (card == null) {
				log.info("Create new card" + raceCard.getMeeting().getCode());
				card = new EventCard();
				card.setProvider("SIS");
				card.setPerfType(PerformanceType.Matinee);
				card.setMessageId(raceCard.getId());
				card.setCardType(getCardType(raceCard));
				card.setCardDate(cardDate);
				card.setTrackCode(raceCard.getSportcode());
				card.setCountryCode(raceCard.getCountry().toString());
				card.setTrackName(raceCard.getMeeting().getName());
			}
			else {
				log.info("Update card" + raceCard.getMeeting().getCode());
			}

			importMeeting(raceCard.getMeeting(), card);

			return card;

		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}
	}

	private CardType getCardType(DataContent data) {
		CardType t = CardType.Thoroughbred;
		if (data.getCategory() != null && data.getCategory() == CategoryEnum.DG) {
			t = CardType.GreyHound;
		}
		return t;
	}

}
