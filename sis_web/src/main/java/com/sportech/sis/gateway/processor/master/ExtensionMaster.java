package com.sportech.sis.gateway.processor.master;

import java.io.StringReader;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.entity.MasterSnapshot;
import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.gateway.processor.ProcessorBase;
import com.sportech.sis.gateway.util.EntityHelper;
import com.sportech.sis.gateway.util.ExtensionImportHelper;

public class ExtensionMaster extends ProcessorBase {

	static private final Logger log = Logger.getLogger(ExtensionMaster.class);

	private ExtensionImportHelper helper = new ExtensionImportHelper();

	@Override
	public String getMessageType() {
		return "EX";
	}

	@Override
	public void execute(ProcessingContext context) {
		MasterSnapshot master = context.getMaster();
		log.info("Got master extension message " + master.getSisId());
		EventCard newCard = helper.parse(context.getEntityManager(), new StringReader(master.getText()));
		if (newCard != null) {
			context.setCard(newCard);
			EntityHelper.updateTimestamp(newCard);
			context.getEntityManager().persist(newCard);
		}
	}

}
