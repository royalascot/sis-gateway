package com.sportech.sis.gateway.util;

import com.sportech.common.model.PoolType;
import com.sportech.common.model.entity.BetInterest;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventPool;
import com.sportech.common.model.entity.EventRace;

public class EntityHelper {
	
	public static EventRace findRace(EventCard card, int num) {
		if (card == null || card.getRaces() == null) {
			return null;
		}
		for (EventRace r : card.getRaces()) {
			if (r.getNumber() != null && r.getNumber() == num) {
				return r;
			}
		}
		return null;
	}
	
	public static BetInterest findRunner(EventRace race, int num) {
		if (race == null || race.getRunners() == null) {
			return null;
		}
		for (BetInterest r : race.getRunners()) {
			if (r.getPosition() != null && r.getPosition() == num) {
				return r;
			}
		}
		return null;
	}
	
	public static EventPool findPool(EventCard card, PoolType pt) {
		if (card == null || card.getPools() == null) {
			return null;
		}
		for (EventPool r : card.getPools()) {
			if (r.getPoolType() != null && r.getPoolType() == pt) {
				return r;
			}
		}
		return null;
	}
	
	public static void updateTimestamp(EventCard c) {
		c.setLastUpdate(System.currentTimeMillis());
	}
	
}
