package com.sportech.sis.gateway.util;

import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.PoolType;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventPool;
import com.sportech.common.model.entity.EventRace;
import com.sportech.common.util.DateHelper;
import com.sportech.sis.common.ImportException;
import com.sportech.sis.common.SisParserHelper;
import com.sportech.sis.gateway.dao.CardDao;
import com.sportech.sis.schema.rs.DataContent;
import com.sportech.sis.schema.rs.EventContent;
import com.sportech.sis.schema.rs.MeetingContent;
import com.sportech.sis.schema.rs.Meetingbet;

public class ResultImportHelper {

	static private final Logger log = Logger.getLogger(ResultImportHelper.class);

	private EventCard card;

	public EventCard parse(EntityManager em, Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(reader), DataContent.class);
			return importCard(em, o.getValue());
		} catch (Exception e) {
			log.error("Error importing result message", e);
		}
		return null;
	}

	private void importPools(DataContent data) {
		MeetingContent meeting = data.getMeeting();
		Set<EventPool> pools = card.getPools();
		if (pools == null) {
			pools = new HashSet<EventPool>();
			card.setPools(pools);
		}
		if (meeting != null) {
			List<Meetingbet> bets = meeting.getMeetingbet();
			for (Meetingbet bet : bets) {
				PoolType code = SisParserHelper.getSisPoolType(bet.getBettype());
				if (code != null) {
					EventPool pool = EntityHelper.findPool(card, code);
					if (pool == null) {
						pool = new EventPool();
						pool.setCode(code.getCode());
						pool.setCard(card);
						pool.setPoolType(code);
					}
					pool.setPoolName(code.getName());
					String raceText = bet.getRaces();
					if (StringUtils.isNotBlank(raceText)) {
						String[] races = raceText.split("-");
						if (races != null && races.length > 1) {
							for (String r : races) {
								pool.getRaces().add(Long.parseLong(r));
							}
							pool.refresh();
							pools.add(pool);
						}
					}
				}
			}
		}
	}

	private void importRace(EventContent race) throws ImportException {

		EventRace hr = EntityHelper.findRace(card, race.getNum().intValue());
		if (hr == null) {
			log.error("Result Set update can not find race " +  race.getNum() + " in card " + card.getSourceId());
			return;
		}

		try {
			if (race.getTime() != null) {
				hr.setPostTime(DateHelper.getPostTime(race.getTime()));
			}
			hr.setRaceNameLong(race.getName());
			if (race.getPlacesExpected() != null) {
				hr.setPlacesExpected(race.getPlacesExpected().intValue());
			}
			hr.setRaceNameShort(StringUtils.abbreviate(race.getName(), 20));
		} catch (Exception e) {
			log.error("Error importing race", e);
			throw new ImportException("Error importing race.");
		}
	}

	private EventCard importCard(EntityManager em, DataContent raceCard) throws ImportException {
		if (raceCard.getMeeting() == null) {
			return null;
		}
		String cardDate = DateHelper.dateToStr(DateHelper.getCardDate(raceCard.getDate()));
		String id = raceCard.getMeeting().getCode();
		card = new CardDao(em).findCardByCode(cardDate, id);
		try {

			if (card == null) {
				log.error("Result update can not find card " + id);
				return null;
			}
			
			for (EventContent e : raceCard.getMeeting().getEvent()) {
				importRace(e);
			}

			importPools(raceCard);

			return card;

		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}
	}

}
