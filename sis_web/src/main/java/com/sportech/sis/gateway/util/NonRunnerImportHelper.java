package com.sportech.sis.gateway.util;

import java.io.Reader;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.entity.BetInterest;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventRace;
import com.sportech.common.util.DateHelper;
import com.sportech.sis.common.ImportException;
import com.sportech.sis.gateway.dao.CardDao;
import com.sportech.sis.schema.nr.DataContent;
import com.sportech.sis.schema.nr.EventContent;
import com.sportech.sis.schema.nr.MeetingContent;
import com.sportech.sis.schema.nr.NonrunnerContent;
import com.sportech.sis.schema.nr.Selection;

public class NonRunnerImportHelper {

	static private final Logger log = Logger.getLogger(NonRunnerImportHelper.class);

	public EventCard parse(EntityManager em, Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(reader), DataContent.class);
			EventCard card = importMeeting(em, o.getValue());
			if (card != null) {
				em.persist(card);
			}
			return card;
		} catch (Exception e) {
			log.error("Error importing non-runner message", e);
		}
		return null;
	}

	private EventCard importMeeting(EntityManager em, DataContent data) {
		try {
			if (data.getMeeting() != null) {
				CardDao dao = new CardDao(em);
				for (MeetingContent meeting : data.getMeeting()) {
					EventCard card = dao.findCardByCode(DateHelper.dateToStr(DateHelper.getCardDate(data.getDate())), meeting.getCode());
					if (card == null) {
						log.error("Can not find card:" + meeting.getCode());
						continue;
					}
					if (meeting.getEvent() != null) {
						for (EventContent ev : meeting.getEvent()) {
							importRace(em, card, ev);
						}
					}
					return card;
				}

			}
		} catch (Exception e) {
			log.error("Error importing non-runner", e);
		}
		return null;
	}

	private void importRace(EntityManager em, EventCard card, EventContent race) throws ImportException {
		EventRace hr = EntityHelper.findRace(card, race.getNum().intValue());
		if (hr == null) {
			log.error("Can not find race:" + race.getNum());
			return;
		}
		for (NonrunnerContent nr : race.getNonrunner()) {
			if (nr.getSelection() != null) {
				for (Selection s : nr.getSelection()) {
					for (BetInterest bi : hr.getRunners()) {
						if (StringUtils.equals(bi.getSourceId(), "" + s.getId())) {
							bi.setScratched(true);
							log.info("Scratched runner " + s.getNum() + ", race " + hr.getNumber() + ", card " + card.getSourceId());
							break;
						}
					}
				}
			}
		}
	}

}
