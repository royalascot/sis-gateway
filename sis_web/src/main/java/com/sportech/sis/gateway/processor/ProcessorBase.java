package com.sportech.sis.gateway.processor;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.actionrule.ProcessingRule;
import com.sportech.common.actionrule.RuleContext;

public abstract class ProcessorBase implements ProcessingRule {
    
    static private final Logger log = Logger.getLogger(ProcessorBase.class);
      
    public abstract void execute(ProcessingContext context);
    
    public abstract String getMessageType();
       
    @Override
    public int getPriority() {
        return 1;
    }

    @Override 
    public boolean isApplicable(RuleContext context) {
    	ProcessingContext pc = (ProcessingContext) context;
        String messageType = pc.getMessage().getMessageType();
        if (StringUtils.equalsIgnoreCase(messageType, getMessageType())) {
        	return true;
        }
        return false;
    }

    @Override 
    public boolean process(RuleContext context) {
    	ProcessingContext pc = (ProcessingContext) context;
        try {
            execute(pc);
        } catch (Exception e) {
        	log.error("Error executing processor", e);
        }
        return false;
    }
    	
}
