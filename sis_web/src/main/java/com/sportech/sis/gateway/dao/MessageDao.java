package com.sportech.sis.gateway.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import com.sportech.sis.entity.MasterSnapshot;
import com.sportech.sis.entity.RawMessage;

public class MessageDao {

	private EntityManager em;

	public MessageDao(EntityManager em) {
		this.em = em;
	}

	public MasterSnapshot findMaster(String messageDate, String sisId) {
		String jql = "FROM MasterSnapshot WHERE messageDate = :messageDate AND sisId = :sisId ORDER BY id DESC";
		TypedQuery<MasterSnapshot> query = em.createQuery(jql, MasterSnapshot.class);
		query.setParameter("messageDate", messageDate);
		query.setParameter("sisId", sisId);
		query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
		List<MasterSnapshot> results = query.getResultList();
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	public List<RawMessage> replayMessages(long startId, long length) {
		String jql = "FROM RawMessage WHERE id > :startId AND id < :endId ORDER BY id";
		TypedQuery<RawMessage> query = em.createQuery(jql, RawMessage.class);
		query.setParameter("startId", startId);
		query.setParameter("endId",  startId + length);
		List<RawMessage> results = query.getResultList();
		return results;
	}

}
