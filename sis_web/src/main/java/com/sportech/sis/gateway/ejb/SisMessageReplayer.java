package com.sportech.sis.gateway.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.sportech.sis.entity.RawMessage;
import com.sportech.sis.gateway.dao.MessageDao;

@Singleton
public class SisMessageReplayer {

	static private final Logger log = Logger.getLogger(SisMessageReplayer.class);

	@PersistenceContext(unitName = "sis")
	EntityManager em;

	@EJB
	private SisMessageSenderBean sender;

	private long lastId = 0L;

	private long length = 1000;

	//@Schedule(second="*/10", minute = "*", hour = "*", persistent = false)
	public void replayMessage() {
		try {
			List<RawMessage> messages = new MessageDao(em).replayMessages(lastId, length);
			lastId += length;
			for (RawMessage raw : messages) {
				sender.sendObject(raw);
			}
		} catch (Exception e) {
			log.error("Error getting message", e);
		}
	}

}
