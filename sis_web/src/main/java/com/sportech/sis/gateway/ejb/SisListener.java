package com.sportech.sis.gateway.ejb;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.ResourceAdapter;

import com.sportech.common.util.ZipHelper;
import com.sportech.sis.common.SisParserHelper;
import com.sportech.sis.entity.RawMessage;
import com.sportech.sis.schema.SisBase;

@MessageDriven(name = "SisListener", activationConfig = {

@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),

@ActivationConfigProperty(propertyName = "messagingType", propertyValue = "javax.jms.MessageListener"),

@ActivationConfigProperty(propertyName = "useJNDI", propertyValue = "false"),

@ActivationConfigProperty(propertyName = "hostName", propertyValue = "93.174.216.174"),

@ActivationConfigProperty(propertyName = "port", propertyValue = "1438"),

@ActivationConfigProperty(propertyName = "maxPoolDepth", propertyValue = "1"),

@ActivationConfigProperty(propertyName = "channel", propertyValue = "S_BETFRDTST1"),

@ActivationConfigProperty(propertyName = "queueManager", propertyValue = "ASEGBFRD5"),

@ActivationConfigProperty(propertyName = "destination", propertyValue = "SPORTSDATA_11"),

@ActivationConfigProperty(propertyName = "sslCipherSuite", propertyValue = "SSL_RSA_WITH_DES_CBC_SHA"),

@ActivationConfigProperty(propertyName = "sslFipsRequired", propertyValue = "false"),

@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),

@ActivationConfigProperty(propertyName = "transportType", propertyValue = "CLIENT")

})

@ResourceAdapter(value = "wmq.jmsra.rar")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@TransactionManagement(TransactionManagementType.CONTAINER)

public class SisListener implements MessageListener {

	static private final Logger log = Logger.getLogger(SisListener.class);

	@PersistenceContext(unitName = "sis")
	EntityManager em;

	@EJB
	private SisMessageSenderBean sender;
	
	public void onMessage(Message message) {
		try {
			final BytesMessage textMessage = (BytesMessage) message;
			int length = (int) textMessage.getBodyLength();
			byte[] data = new byte[length];
			textMessage.readBytes(data, length);
			String text = new String(data);
			JAXBContext jc = JAXBContext.newInstance(SisBase.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<SisBase> o = unmarshaller.unmarshal(new StreamSource(is), SisBase.class);
			SisBase baseData = o.getValue();

			RawMessage raw = new RawMessage();
			raw.setText(text);
			raw.setData(ZipHelper.deflateBuffer(data));
			raw.setCountry(baseData.getCountry());
			raw.setMessageTimestamp(baseData.getTimestamp());
			raw.setCategory(SisParserHelper.findCategory(baseData.getType()));
			raw.setMessageType(baseData.getMnem());
			raw.setRaceType(baseData.getCategory());
			raw.setImportTime(new Date());
			raw.setSisId(baseData.getId());
			raw.setMessageDate(baseData.getDate());
			raw.setMessageId(message.getJMSMessageID());
			em.persist(raw);
			sender.sendObject(raw);
		} catch (Exception e) {
			log.error("Error getting message", e);
		}
	}
	
}