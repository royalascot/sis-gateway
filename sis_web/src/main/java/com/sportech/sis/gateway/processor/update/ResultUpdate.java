package com.sportech.sis.gateway.processor.update;

import java.io.StringReader;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.gateway.processor.ProcessingContext;
import com.sportech.sis.gateway.processor.ProcessorBase;
import com.sportech.sis.gateway.util.EntityHelper;
import com.sportech.sis.gateway.util.ResultImportHelper;
import com.sportech.sis.gateway.util.UpdateHelper;

public class ResultUpdate extends ProcessorBase {

	static private final Logger log = Logger.getLogger(ResultUpdate.class);

	@Override
	public String getMessageType() {
		return "RS";
	}

	@Override
	public void execute(ProcessingContext context) {
		try {
			if (UpdateHelper.applyUpdate(context)) {
				ResultImportHelper helper = new ResultImportHelper();
				EventCard newCard = helper.parse(context.getEntityManager(), new StringReader(context.getMaster().getText()));
				if (newCard != null) {
					context.setCard(newCard);
					EntityHelper.updateTimestamp(newCard);
					context.getEntityManager().persist(newCard);
				}
				else {
					log.warn("Empty update action.");
				}
			}
		} catch (Exception e) {
			log.error("Error applying update message", e);
		}

	}

}
