package com.sportech.sis.gateway.ejb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.sportech.common.model.entity.EventCard;
import com.sportech.sis.gateway.bi.CardUpdateSender;
import com.sportech.sis.gateway.dao.CardDao;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class UpdateMonitor {

    static private final Logger log = Logger.getLogger(UpdateMonitor.class);

	@PersistenceContext(unitName = "sis")
	EntityManager em;
	
	@EJB
	CardUpdateSender updateSender;
	
	private static final long lastUpdateOffset = 60000L;
	
	private CardDao dao = null;
	
	@PostConstruct
	private void init() {
		dao = new CardDao(em);
	}
	
    @Schedule(minute = "*", hour = "*", persistent = false)
    @Lock(LockType.WRITE)
    public void checkUpdates() {
    	long lastUpdate = System.currentTimeMillis() - lastUpdateOffset;
        try {
        	List<EventCard> cards = dao.getCardsByTimestamp(lastUpdate);
        	for (EventCard c : cards) {  
        		EventCard full = dao.getFullCard(c.getId());
        		updateSender.sendObject(full);
        	}
        } catch (Exception ex) {
            log.error("Error sending updates", ex);
        }
    }

}
