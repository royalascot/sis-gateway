package com.sportech.sis.gateway.ejb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

@Stateless
@LocalBean
public class SisMessageSenderBean {

	static private final Logger log = Logger.getLogger(SisMessageSenderBean.class);

	@Resource(mappedName = "java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/sisgateway")
	private Destination queue;

	private Connection connection;
	private Session session = null;
	private MessageProducer sender = null;

	@PostConstruct
	public void init() {
		connection = null;
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			sender = session.createProducer(queue);
			sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		} catch (Exception e) {
			log.error("Error connecting to queue", e);
		}
	}

	@PreDestroy
	public void destroy() {
		try {
			if (sender != null) {
				sender.close();
			}
			if (session != null) {
				session.close();
			}
		} catch (Exception e) {
			log.error("error close", e);
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				log.error("Error closing connetion", e);
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void send(String text) {
		try {
			TextMessage response = session.createTextMessage(text);
			sender.send(response);
		} catch (Exception e) {
			log.error("Error sending message", e);
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendObject(Serializable text) {
		try {
			ObjectMessage response = session.createObjectMessage(text);
			sender.send(response);
		} catch (Exception e) {
			log.error("Error sending message", e);
		}
	}

}