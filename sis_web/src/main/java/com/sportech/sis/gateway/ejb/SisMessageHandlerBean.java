package com.sportech.sis.gateway.ejb;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.sportech.sis.entity.MessageCategory;
import com.sportech.sis.entity.RawMessage;
import com.sportech.sis.gateway.processor.MasterProcessor;
import com.sportech.sis.gateway.processor.UpdateProcessor;

@MessageDriven(activationConfig = {

@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),

@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1"),

@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/sisgateway")

})
public class SisMessageHandlerBean implements MessageListener {

	static private final Logger log = Logger.getLogger(SisMessageHandlerBean.class);

	@PersistenceContext(unitName = "sis")
	EntityManager em;

	private MasterProcessor master = new MasterProcessor();
	private UpdateProcessor update = new UpdateProcessor();
	
	@AccessTimeout(value = 10, unit = TimeUnit.MINUTES)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void onMessage(Message message) {
		try {
			final ObjectMessage textMessage = (ObjectMessage) message;
			if (textMessage != null) {
				process(textMessage.getObject());
			}
		} catch (Exception e) {
			log.error("Error processing message", e);
		}
	}

	private void process(Serializable object) {
		try {
			RawMessage message = (RawMessage) object;
			if (message.getCategory() == MessageCategory.Master) {
				master.process(em, message);
			} else {
				update.process(em, message);
			}
		} catch (Exception e) {
			log.error("Error processing sis message", e);
		}
	}

}
