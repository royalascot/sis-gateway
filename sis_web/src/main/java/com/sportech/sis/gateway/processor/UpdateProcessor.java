package com.sportech.sis.gateway.processor;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.sportech.common.actionrule.RuleExecutor;
import com.sportech.sis.entity.RawMessage;

public class UpdateProcessor {

	static private final Logger log = Logger.getLogger(UpdateProcessor.class);
	
	private RuleExecutor<ProcessorBase> executor = null;

	public UpdateProcessor() {
		executor = new RuleExecutor<ProcessorBase>("com.sportech.sis.gateway.processor.update", ProcessorBase.class);
	}

	public void process(EntityManager em, RawMessage message) {
		ProcessingContext context = new ProcessingContext();
		context.setMessage(message);
		context.setEntityManager(em);
		
		int ruleCount = executor.execute(context);
		if (ruleCount < 1) {
			log.warn("No updater found for message type: " + message.getMessageType());
		}
	}

}
