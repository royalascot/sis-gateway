package com.sportech.sis.gateway.util;

import java.io.Reader;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.entity.BetInterest;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventRace;
import com.sportech.common.util.DateHelper;
import com.sportech.sis.common.ImportException;
import com.sportech.sis.common.SisParserHelper;
import com.sportech.sis.gateway.dao.CardDao;
import com.sportech.sis.schema.ex.DataContent;
import com.sportech.sis.schema.ex.EventContent;
import com.sportech.sis.schema.ex.Price;
import com.sportech.sis.schema.ex.SelectionContent;

public class ExtensionImportHelper {

	static private final Logger log = Logger.getLogger(ExtensionImportHelper.class);

	public EventCard parse(EntityManager em, Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(reader), DataContent.class);
			EventCard card = importCard(em, o.getValue());
			return card;
		} catch (Exception e) {
			log.error("Error importing event message", e);
		}
		return null;
	}

	private BetInterest importRunner(EventRace race, SelectionContent starter) throws ImportException {
		BetInterest runner = EntityHelper.findRunner(race, Integer.parseInt(starter.getNum()));
		if (runner == null) {
			runner = new BetInterest();
			runner.setPosition(Long.parseLong(starter.getNum()));
			runner.setRace(race);
			race.getRunners().add(runner);
		}

		try {

			runner.setSourceId("" + starter.getId());
			runner.setJockeyName(starter.getJockey());
			runner.setName(starter.getName());
			if (StringUtils.isNotBlank(starter.getShortName())) {
				runner.setShortName(starter.getShortName());
			} else {
				runner.setShortName(SisParserHelper.getRunnerShortName(runner.getName()));
			}

			for (Price p : starter.getPrice()) {
				log.info("Update prices from extension message:" + starter.getId());
				runner.setDec(p.getDec());
				runner.setFract(p.getFract());
				runner.setMornlineOdds(p.getFract());
				if (p.getTimestamp() != null) {
					runner.setPriceTimestamp(p.getTimestamp().longValue());
				}
				runner.setMarketType(p.getMkttype());
			}

			return runner;

		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private void importRace(EventCard card, EventContent race) {
		EventRace hr = EntityHelper.findRace(card, race.getNum().intValue());
		if (hr == null) {
			log.error("Can not find race " + race.getNum() + " for card " + card.getName());
		}
		try {
			for (SelectionContent s : race.getSelection()) {
				importRunner(hr, s);
			}
		} catch (Exception e) {
			log.error("Error importing race", e);
		}
	}

	private EventCard importCard(EntityManager em, DataContent raceCard) throws ImportException {
		if (raceCard.getMeeting() == null) {
			return null;
		}
		String cardDate = DateHelper.dateToStr(DateHelper.getCardDate(raceCard.getDate()));
		String id = raceCard.getMeeting().getCode();
		EventCard card = new CardDao(em).findCardByCode(cardDate, id);
		try {
			if (card == null) {
				log.error("Extension update can not find card " + id);
				return null;
			}
			importRace(card, raceCard.getMeeting().getEvent());
		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}
		return card;
	}

}
