package com.sportech.sis.schema;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.sis.schema.es.DataContent;

public class MasterTest {
	
	@Test
	public void testEventMaster() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426051803807.xml");
		try {
			byte[] data = IOUtils.toByteArray(stream);
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(is), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("master".equals(baseData.getType()));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
}
