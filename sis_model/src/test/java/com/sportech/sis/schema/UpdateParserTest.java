package com.sportech.sis.schema;

import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Element;

import com.sportech.sis.schema.xup.CommandContent;
import com.sportech.sis.schema.xup.DataContent;

public class UpdateParserTest {

	@Test
	public void testMungeParser() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426079130931.xml");
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(stream), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("update".equals(baseData.getType()));
			Assert.assertTrue(StringUtils.equals(baseData.getDataContent().getName().getLocalPart(), "munge"));
			CommandContent cc = (CommandContent) baseData.getDataContent().getValue();
			List<Object> elements = cc.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					Assert.assertTrue(oo != null);
				} else {
					Element e = (Element) oo;
					Assert.assertTrue(e != null);
				}
			}

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testAttributeUpdateParser() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426058715639.xml");
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(stream), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("update".equals(baseData.getType()));
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testAppendParser() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426079130519.xml");
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(stream), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("update".equals(baseData.getType()));
			Assert.assertTrue(StringUtils.equals(baseData.getDataContent().getName().getLocalPart(), "append"));
			CommandContent cc = (CommandContent) baseData.getDataContent().getValue();
			List<Object> elements = cc.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					Assert.assertTrue(oo != null);
				} else {
					Element e = (Element) oo;
					String text = e.getTextContent();
					Assert.assertTrue(text != null);
				}
			}

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testDelParser() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KH3_1426060419180.xml");
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(stream), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("update".equals(baseData.getType()));
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
