package com.sportech.sis.schema;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

public class GenericSchemaTest {
	
	@Test
	public void testEvent() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("sample master.xml");
		try {
			byte[] data = IOUtils.toByteArray(stream);
			JAXBContext jc = JAXBContext.newInstance(SisBase.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<SisBase> o = unmarshaller.unmarshal(new StreamSource(is), SisBase.class);
			SisBase baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("master".equals(baseData.getType()));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
}
