package com.sportech.sis.schema;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import com.sportech.sis.schema.es.DataContent;
import com.sportech.sis.xmlupdater.XmlUpdater;

public class NonRunnerTest {

	@Test
	public void testNonRunnerMaster() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("HRNR2015-03-11_1426051801765.xml");
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(stream), DataContent.class);
			DataContent baseData = o.getValue();
			Assert.assertTrue(baseData != null);
			Assert.assertTrue("master".equals(baseData.getType()));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void testNonRunnerMunge() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("HRNR2015-03-11_1426051801765.xml");
		InputStream updateStream = ClassLoader.getSystemResourceAsStream("HRNR2015-03-11_1426057815463.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);

			XmlUpdater updater = new XmlUpdater();
			updater.updateMaster(document, updateStream);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			String resultXml = writer.getBuffer().toString();
			Assert.assertTrue(StringUtils.contains(resultXml, "<nonrunner>"));
			Assert.assertTrue(!StringUtils.contains(resultXml, "__key"));
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
