package com.sportech.sis.schema;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import com.sportech.sis.xmlupdater.XmlUpdater;

public class UpdaterTest {

	@Test
	public void testAttributeUpdate() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426051803807.xml");
		InputStream updateStream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426058715639.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);

			XmlUpdater updater = new XmlUpdater();
			updater.updateMaster(document, updateStream);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			String resultString = "<event courseType=\"H\" coverageCode=\"T\" distance=\"2m 5f\" eachWayPlaces=\"3\" going=\"GD/SF(GD PL)\" handicap=\"N\" id=\"2149967\"";
			String converted = writer.getBuffer().toString();
			Assert.assertTrue(StringUtils.contains(converted, resultString));
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testAppend() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426051803807.xml");
		InputStream updateStream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426079130519.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);

			XmlUpdater updater = new XmlUpdater();
			updater.updateMaster(document, updateStream);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			Assert.assertTrue(StringUtils.contains(writer.getBuffer().toString(), "<market"));
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testMunge() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426051803807.xml");
		InputStream updateStream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KG1_1426079130931.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);

			XmlUpdater updater = new XmlUpdater();
			updater.updateMaster(document, updateStream);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			Assert.assertTrue(StringUtils.contains(writer.getBuffer().toString(), "<price"));
			Assert.assertTrue(!StringUtils.contains(writer.getBuffer().toString(), "__key"));
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testDel() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KH3_1426051804008.xml");
		InputStream updateStream = ClassLoader.getSystemResourceAsStream("UKHRES5G9KH3_1426060419180.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);

			XmlUpdater updater = new XmlUpdater();
			updater.updateMaster(document, updateStream);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			Assert.assertTrue(!StringUtils.contains(writer.getBuffer().toString(), "claiming=\"(5)\""));
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
