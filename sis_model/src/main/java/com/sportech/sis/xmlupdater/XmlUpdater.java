package com.sportech.sis.xmlupdater;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sportech.sis.schema.xup.At;
import com.sportech.sis.schema.xup.CommandContent;
import com.sportech.sis.schema.xup.DataContent;
import com.sportech.sis.schema.xup.Del;
import com.sportech.sis.schema.xup.UpsAtCommandContent;

public class XmlUpdater {

	static private final Logger log = Logger.getLogger(XmlUpdater.class);

	private static final String KEY = "__key";

	private interface UpdateAction {
		public void update(Document doc, Node node);
	}

	private class AttributeUpdater implements UpdateAction {

		private UpsAtCommandContent upsat;

		public AttributeUpdater(UpsAtCommandContent upsat) {
			this.upsat = upsat;
		}

		public void update(Document doc, Node n) {
			for (At at : upsat.getAt()) {
				String atName = at.getName();
				((Element) n).setAttribute(atName, at.getContent());
			}
		}
	}

	private class DelUpdater implements UpdateAction {
		public void update(Document doc, Node n) {
			if (n != null) {
				if (n instanceof Attr) {
					Attr a = (Attr) n;
					a.getOwnerElement().removeAttributeNode(a);
				} else {
					Node p = n.getParentNode();
					if (p != null) {
						p.removeChild(n);
					}
				}
			}
		}
	}

	private class AppendUpdater implements UpdateAction {

		private CommandContent content;

		public AppendUpdater(CommandContent c) {
			this.content = c;
		}

		public void update(Document doc, Node n) {
			List<Object> elements = content.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					continue;
				} else {
					Element e = (Element) oo;
					Node newNode = doc.importNode(e, true);
					n.appendChild(newNode);
				}
			}
		}
	}

	private class InsertUpdater implements UpdateAction {

		private CommandContent content;

		private boolean isBefore = false;

		public InsertUpdater(CommandContent c, boolean isBefore) {
			this.content = c;
			this.isBefore = isBefore;
		}

		public void update(Document doc, Node n) {
			List<Object> elements = content.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					continue;
				} else {
					Element e = (Element) oo;
					Node newNode = doc.importNode(e, true);
					if (isBefore) {
						n.getParentNode().insertBefore(newNode, e);
					} else {
						Node next = n.getNextSibling();
						if (next == null) {
							n.getParentNode().appendChild(newNode);
						} else {
							n.getParentNode().insertBefore(newNode, next);
						}
					}
				}
			}
		}
	}

	private class UpdateUpdater implements UpdateAction {

		private CommandContent content;

		public UpdateUpdater(CommandContent c) {
			this.content = c;
		}

		public void update(Document doc, Node n) {
			List<Object> elements = content.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					continue;
				} else {
					List<Node> nodes = new ArrayList<Node>();
					for (int i = 0; i < n.getChildNodes().getLength(); i++) {
						nodes.add(n.getChildNodes().item(i));
					}
					for (Node c : nodes) {
						n.removeChild(c);
					}
					Element e = (Element) oo;
					Node newNode = doc.importNode(e, true);
					n.appendChild(newNode);
				}
			}
		}
	}

	private class MungeUpdater implements UpdateAction {

		private CommandContent content;

		public MungeUpdater(CommandContent c) {
			this.content = c;
		}

		public void update(Document doc, Node n) {
			List<Object> elements = content.getContent();
			for (Object oo : elements) {
				if (oo instanceof String) {
					continue;
				} else {
					Element e = (Element) oo;
					Element newNode = (Element) doc.importNode(e, true);
					Element src = (Element) n;
					apply(doc, src, newNode);
				}
			}
		}

		private void apply(Document doc, Element src, Element toMunge) {
			if (src == null || toMunge == null) {
				return;
			}
			Element matched = null;
			for (int j = 0; j < src.getChildNodes().getLength(); j++) {
				Node m = src.getChildNodes().item(j);
				if (m instanceof Element) {
					Element me = (Element) m;
					if (areSameNodes(me, toMunge)) {
						matched = me;
						break;
					}
				}
			}
			if (matched != null) {
				for (int i = 0; i < toMunge.getChildNodes().getLength(); i++) {
					Node t = toMunge.getChildNodes().item(i);
					if (t instanceof Element) {
						apply(doc, matched, (Element) t);
					}
				}
			} else {
				removeKey(toMunge);
				src.appendChild(toMunge);
			}
		}

		private boolean areSameNodes(Element src, Element munge) {
			if (!StringUtils.equalsIgnoreCase(src.getNodeName(), munge.getNodeName())) {
				return false;
			}
			if (!StringUtils.isEmpty(munge.getAttribute(KEY))) {
				String key = munge.getAttribute(KEY);
				String mungeId = munge.getAttribute(key);
				if (StringUtils.isNotEmpty(mungeId)) {
					String srcId = src.getAttribute(key);
					if (!StringUtils.equalsIgnoreCase(srcId, mungeId)) {
						return false;
					}
				}
			}
			return true;
		}

		private void removeKey(Element munge) {
			munge.removeAttribute(KEY);
			for (int i = 0; i < munge.getChildNodes().getLength(); i++) {
				Node t = munge.getChildNodes().item(i);
				if (t instanceof Element) {
					removeKey((Element) t);
				}
			}
		}
	}

	public void updateMaster(Document master, InputStream stream) {
		DataContent update = getUpdateContent(new InputStreamReader(stream));
		if (update != null) {
			updateMaster(master, update.getDataContent());
		}
	}

	public void updateMaster(Document master, String xml) {
		DataContent update = getUpdateContent(new StringReader(xml));
		if (update != null) {
			updateMaster(master, update.getDataContent());
		}	
	}
	
	private DataContent getUpdateContent(Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(DataContent.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<DataContent> o = unmarshaller.unmarshal(new StreamSource(reader), DataContent.class);
			DataContent baseData = o.getValue();
			return baseData;
		} catch (Exception e) {
			log.error("Error parsing update message", e);
		}
		return null;
	}

	private <T> void updateMaster(Document master, JAXBElement<T> update) {

		String name = update.getDeclaredType().getSimpleName();
		if (name.equals("Del")) {
			Del upsat = (Del) update.getValue();
			DelUpdater updater = new DelUpdater();
			updateNodes(master, upsat.getSelect(), updater);
		} else if (name.equals("UpsAtCommandContent")) {
			UpsAtCommandContent upsat = (UpsAtCommandContent) update.getValue();
			AttributeUpdater updater = new AttributeUpdater(upsat);
			updateNodes(master, upsat.getSelect(), updater);
		} else if (name.equals("CommandContent")) {
			CommandContent cc = (CommandContent) update.getValue();
			if (StringUtils.equals(update.getName().getLocalPart(), "append")) {
				AppendUpdater updater = new AppendUpdater(cc);
				updateNodes(master, cc.getSelect(), updater);
			} else if (StringUtils.equals(update.getName().getLocalPart(), "munge")) {
				MungeUpdater updater = new MungeUpdater(cc);
				updateNodes(master, cc.getSelect(), updater);
			} else if (StringUtils.equals(update.getName().getLocalPart(), "update")) {
				UpdateUpdater updater = new UpdateUpdater(cc);
				updateNodes(master, cc.getSelect(), updater);
			} else if (StringUtils.equals(update.getName().getLocalPart(), "ins-pre")) {
				InsertUpdater updater = new InsertUpdater(cc, true);
				updateNodes(master, cc.getSelect(), updater);
			} else if (StringUtils.equals(update.getName().getLocalPart(), "ins-post")) {
				InsertUpdater updater = new InsertUpdater(cc, false);
				updateNodes(master, cc.getSelect(), updater);
			}
		}
	}

	private void updateNodes(Document master, String select, UpdateAction action) {
		try {

			XPath xpath = XPathFactory.newInstance().newXPath();
			String s = StringUtils.replace(select, "hrdg:", "");
			NodeList nodes = (NodeList) xpath.evaluate(s, master, XPathConstants.NODESET);

			for (int idx = 0; idx < nodes.getLength(); idx++) {
				Node n = nodes.item(idx);
				action.update(master, n);
			}

		} catch (Exception e) {
			log.error("Error applying update", e);
		}
	}

}