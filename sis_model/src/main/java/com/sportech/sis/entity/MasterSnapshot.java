package com.sportech.sis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "sis_master",
indexes = {@Index(name = "master_index_date_id", columnList="message_date,sis_id", unique = false)})
public class MasterSnapshot {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    private String text;
    
    @Column(name = "message_type", length=20)
    private String messageType;
    
    @Column(name = "race_type", length=20)
    private String raceType;
  
    @Column(name = "last_update")
    private Long lastUpdate;

    @Column(name = "sis_id", length=200)
    private String sisId;
    
    @Column(name = "message_date", length=30)
    private String messageDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getRaceType() {
		return raceType;
	}

	public void setRaceType(String raceType) {
		this.raceType = raceType;
	}

	public Long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getSisId() {
		return sisId;
	}

	public void setSisId(String sisId) {
		this.sisId = sisId;
	}

	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}
    
}
