package com.sportech.sis.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "sis_message")
public class RawMessage implements Serializable {

	private static final long serialVersionUID = 3393943540596377796L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    private byte[] data;
    
    @Lob
    private String text;
    
    @Column(name = "message_id", length=200)
    private String messageId;
    
    @Column(name = "message_type", length=20)
    private String messageType;
    
    @Column(name = "race_type", length=20)
    private String raceType;

    @Column(name = "country", length=20)
    private String country;

    @Basic
    private MessageCategory category;
    
    @Column(name = "message_timestamp")
    private Long messageTimestamp;
    
    @Column(name="import_time")
    private Date importTime;

    @Column(name="message_date", length=30)
    private String messageDate;
    
    @Column(name="sis_id", length=200)
    private String sisId;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public MessageCategory getCategory() {
		return category;
	}

	public void setCategory(MessageCategory category) {
		this.category = category;
	}

	public Date getImportTime() {
		return importTime;
	}

	public void setImportTime(Date importTime) {
		this.importTime = importTime;
	}

	public Long getMessageTimestamp() {
		return messageTimestamp;
	}

	public void setMessageTimestamp(Long messageTimestamp) {
		this.messageTimestamp = messageTimestamp;
	}

	public String getRaceType() {
		return raceType;
	}

	public void setRaceType(String raceType) {
		this.raceType = raceType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	public String getSisId() {
		return sisId;
	}

	public void setSisId(String sisId) {
		this.sisId = sisId;
	}
    
}
