package com.sportech.sis.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.PoolType;
import com.sportech.common.model.RunnerContainer;
import com.sportech.common.model.entity.PoolLeg;
import com.sportech.common.model.entity.PoolList;
import com.sportech.common.util.JSonHelper;
import com.sportech.sis.entity.MessageCategory;
import com.sportech.sis.schema.es.EventContent;
import com.sportech.sis.schema.es.RacebetContent;

@SuppressWarnings("serial")
public class SisParserHelper {

    static private final Logger log = Logger.getLogger(SisParserHelper.class);

    private static Map<String, Integer> poolMap = new HashMap<String, Integer>() {
        {
            put("W", 1);
            put("P", 2);
            put("X", 4);
            put("I", 6);
        }
    };

    private static Map<String, PoolType> multiLegPoolMap = new HashMap<String, PoolType>() {
        {
            put("J", PoolType.PK6);
            put("L",  PoolType.VPK);
            put("Q", PoolType.PK4);
        }
    };

    private static Map<String, PoolType> betfredPoolMap = new HashMap<String, PoolType>() {
        {
            put("Win", PoolType.WIN);
            put("Place", PoolType.PLC);
            put("Exacta", PoolType.EXA);
            put("Trifecta", PoolType.TRI);
            put("Jackpot", PoolType.VPK);
            put("Double", PoolType.DBL);
            put("Tote Double", PoolType.DBL);
            put("Treble", PoolType.PK3);
            put("Tote Treble", PoolType.PK3);
            put("Quadpot", PoolType.PK4);
            put("Placepot", PoolType.PK6);
            put("Swinger", PoolType.OMN);
            put("Scoop6", PoolType.PKX);
        }
    };

    private static Map<String, String> subcodeMap = new HashMap<String, String>() {
        {
            // Horse
            put("NH", "National Hunt Meeting");
            put("FL", "Flat Meeting");
            put("CO", "Combined Meeting");
            put("TR", "Trotting");

            // Dog
            put("NB", "Non-BAGS (evening) meeting");
            put("BA", "BAGS meeting");
            put("VR", "Virtual meeting");
            put("NA", "None of the above");
        }
    };

    public static String getRaceCode(String subCode) {
        String raceCode = "";
        if (StringUtils.isNotBlank(subCode)) {
            String rc = subcodeMap.get(subCode);
            if (rc != null) {
                raceCode = rc;
            } else {
                raceCode = subCode;
            }
        }
        return raceCode;
    }

    public static void parsePool(EventContent event, RunnerContainer race) {
        if (event.getRacebet() != null) {
            List<Integer> pools = new ArrayList<Integer>();
            for (RacebetContent b : event.getRacebet()) {
                if (StringUtils.isNotEmpty(b.getBettype())) {
                    Integer i = poolMap.get(b.getBettype());
                    if (i != null) {
                        pools.add(i);
                    }
                }
            }
            if (pools.size() > 0) {
                List<PoolLeg> legs = new ArrayList<PoolLeg>();
                PoolList pl = new PoolList();
                pl.setLegs(legs);
                PoolLeg leg = new PoolLeg();
                leg.setLegNumber(1);
                leg.setPoolIds(pools.toArray(new Integer[0]));
                legs.add(leg);
                String json = JSonHelper.toJSon(pl);
                race.setPoolIdList(json);
            }
        }
    }

    public static PoolType getBetfredPool(String name) {
        PoolType i = betfredPoolMap.get(name);
        if (i == null) {
            log.error("Unknown betfred pool name:" + name);
        }
        return i;
    }

    public static PoolType getSisPoolType(String text) {
        if (text != null) {
            PoolType c = multiLegPoolMap.get(text);
            return c;
        }
        return null;
    }

    public static String normalizeCountryCode(String country) {
        String countryCode = "UK";
        if (StringUtils.isNotBlank(country)) {
            countryCode = StringUtils.trim(country);
            if (StringUtils.equalsIgnoreCase(countryCode, "IR")) {
                countryCode = "IE";
            }
        }
        return countryCode;
    }
    
    public static String getRunnerShortName(String name) {
	    return StringUtils.abbreviate(name, 18);
	}
    
    public static MessageCategory findCategory(String c) {
    	if (StringUtils.equalsIgnoreCase(c,  "master")) {
    		return MessageCategory.Master;
    	}
    	return MessageCategory.Update;
    }
    
}


