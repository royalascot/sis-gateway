package com.sportech.sis.common;

@SuppressWarnings("serial")
public class ImportException extends Exception {

	public ImportException(String message) {
		super(message);
	}

}
