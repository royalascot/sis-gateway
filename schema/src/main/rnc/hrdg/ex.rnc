#
# Event (Extended) Schema definition (mnemonic EX)
# John Watson                                   
# 03/11/2011                                    
#
#

namespace local = ""
namespace e = "http://www.satelliteinfo.co.uk/feed/editability"
# this is the namespace for horse and greyhound racing
default namespace target = "http://www.satelliteinfo.co.uk/feed/master/hrdg"
datatypes xsd = "http://www.w3.org/2001/XMLSchema-datatypes"


grammar {
    include "commonattrib.rnc"
    include "category.rnc"
    include "country.rnc"
    include "bool.rnc"
    include "feedsource.rnc"
    include "settlingstatustype.rnc"  
    include "inrunningstatusType.rnc"
    
    start = element data { data-content }
    
    # the root element is always named 'data' 
    # within the target namespace (the default namespace)
    data-content = 
        common-root-attributes,        
        attribute mnem { "EX" },
        attribute name { "event_extra" },
        attribute country { country-enumeration },
        attribute sportcode { text }?,
        element meeting { meeting-content }   

       
    # a meeting
    meeting-content =
         attribute name { text },
         attribute shortName { text },
         attribute code { text },     
         attribute category { category-enumeration },    
         attribute country { country-enumeration },
         attribute date { xsd:date },
         attribute going { text }?,
         attribute status { text }?,
         attribute sportcode { text }?,
         attribute subcode { text }?,
         attribute sourceId { text }?,
         element event { event-content }   
         
    # a race
    event-content = 
         attribute id { xsd:integer },
         attribute name { text },
         attribute title { text }?,
         attribute num { xsd:integer },
         attribute time { xsd:time },
         element selection { selection-content }+,
         element result { result-content },
         element market { market-content }?
         
    # a runner
    selection-content =          
         attribute id { xsd:integer },
         attribute name { text },
         attribute shortName { text },
         attribute num { text },  
         attribute jockey { text },
         attribute shortJockey { text },
         element price { price-content }*
         
    # a bar price
    bar-content =
         element price { price-content }*
         
    # a race result
    result-content =          
         attribute id { xsd:integer },
         attribute comments { text }?,
         attribute favouritenews { text }?,
         attribute message { text }?,
         attribute nonrunners { xsd:integer }?,
         attribute settlingstatus { settlingstatus-enumeration }?,
         attribute statuscode { text }?,
         attribute weighedIn { xsd:time }?,
         element position { position-content }*
 
  
    # a runner's odds
    price-content = 
         attribute id { xsd:integer },
         attribute dec { xsd:decimal },
         attribute fract { text },
         attribute mkttype { text },
         attribute time { xsd:time },
         attribute timestamp { xsd:integer }
    
    # a result position
    position-content = 
         attribute id { xsd:integer },
         attribute name { text },
         attribute num { xsd:integer },
         attribute runnernumber { xsd:integer },
         attribute selectionref { xsd:integer }?,
         attribute trainer { text }?,
         (
           ( # finisher
           attribute position { xsd:integer },
           attribute deadheat { text }?,
           attribute winnersTime { xsd:time }?,
           attribute beatenDistance{ text }?,
           attribute photo { bool-enumeration },
           element photoselection { photoselection-content }*
           )
           | 
           ( # did not finish
           attribute status { in-running-status-enumeration }
           )
         )
         
            
    # a betting market
    market-content = 
         attribute id { xsd:integer },
         attribute mkttype { "I" },
         attribute supplier { text },
         attribute time { xsd:time },
         attribute fieldmoney { xsd:decimal }?
         
     # a photo finish for a result place
    photoselection-content = 
         attribute id { xsd:integer },
         attribute name { text },
         attribute num { xsd:integer },    
         attribute selectionid { xsd:integer }
                  
    }