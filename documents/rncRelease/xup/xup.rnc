#
# XUP Schema
# John Watson                                   
# 22/04/2004                                    
#
# Version 1.0.1  21/03/2005.  Add optional country attribute to data content.
#

# xu is the namespace prefix for XUP
namespace xu = "http://www.satelliteinfo.co.uk/feed/update"
namespace local = ""
# this is the namespace for the target document.  Replace the URI with whatever is appropriate
default namespace target = "http://www.satelliteinfo.co.uk/feed/master/hrdg"
datatypes xsd = "http://www.w3.org/2001/XMLSchema-datatypes"

grammar {
    include "country.rnc"
    include "incrementname.rnc"

    start = element data { data-content }
    
    # the root element is always named 'data' with a type of 'update' and belongs
    # within the target namespace (the default namespace).
    #    id is the identifier of the target document.  In rare cases this may be a space-delimited list of targets.
    #    date is the meeting/event date of the target document.
    #    group is the publisher group that owns the increment. If absent, it is a basic Sports Data increment.
    #    category is the sport category code of the target document.
    #    version is the version of the SD feed handler that has emitted the increment.
    #    country is the ISO country code
    #    name is the name of the increment (where used)
    #    route is for internal use only, to aid document routing
    #    mnem is the mnemonic of the target document(s)
    data-content = 
        attribute id { text },
        attribute type { "update" },
        attribute date  { xsd:date },
        attribute timestamp { text },
        attribute group  { text }?,
        attribute source { text },  
        attribute category  { text },
        attribute version  { text },
        attribute country { country-enumeration },                 
        attribute name  { increment-name-enumeration },
        attribute route  { text },
        attribute mnem  { text }?,
        element-command 
        
    # the various XUP commands
    element-command = 
         element xu:append { command-content } |
         element xu:ins-pre { command-content } |
         element xu:ins-post { command-content } |
         element xu:update { command-content } |
         element xu:del { del-command-content } |
         element xu:ins { command-content } |
         element xu:munge { command-content } |
         element xu:ups-at { ups-at-command-content }
       
    # standard commands have a select XPath expression and an arbitrary XML fragment
    command-content =
         attribute select { text },
         target-payload
         
    # the ups-at command uses an XPath expression and a set of target attributes
    ups-at-command-content =
         attribute select { text },
         ups-at-target +   
         
    # the delete command has a select XPath expression but no body
    del-command-content =
         attribute select { text }
         
    # the targe attributes to be upserted
    ups-at-target = 
         element xu:at { attribute name { text }, text }

    # target payload describes an arbitrary XML fragment that inhabits the target namespace         
    target-payload = element target:* { target-content} | text
    target-content = ( element target:* { target-content } | attribute * { text } | text )*
    }